﻿using System;
using System.Windows.Forms;
using Mobies.Forms;

namespace Mobies
{
    public partial class MainForm : Form
    {
        private readonly LoginForm lf = new LoginForm();
        private CompareForm cf = new CompareForm { TopLevel = false, AutoScroll = true };
        private SearchForm sf = new SearchForm { TopLevel = false, AutoScroll = true };

        public MainForm(string userName)
        {
            InitializeComponent();
            txtUserLoged.Text = userName;
        }

        private void BtnLogout_Click(object sender, EventArgs e)
        {
            txtUserLoged.ResetText();
            this.Visible = false;
            lf.Visible = true;
        }

        private void BtnCompare_Click(object sender, EventArgs e)
        {
            cf = new CompareForm { TopLevel = false, AutoScroll = true };

            sf.Visible = false;
            panelContainer.Controls.Add(cf);
            cf.FormBorderStyle = FormBorderStyle.None;
            cf.Dock = DockStyle.Fill;
            cf.Show();
        }

        private void BtnSearchMovie_Click(object sender, EventArgs e)
        {
            sf = new SearchForm { TopLevel = false, AutoScroll = true };

            cf.Visible = false;
            panelContainer.Controls.Add(sf);
            sf.FormBorderStyle = FormBorderStyle.None;
            sf.Dock = DockStyle.Fill;
            sf.Show();
        }

        private void BtnSair_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
