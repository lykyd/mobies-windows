﻿namespace Mobies
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbLoged = new System.Windows.Forms.GroupBox();
            this.btnLogout = new System.Windows.Forms.Button();
            this.txtUserLoged = new System.Windows.Forms.Label();
            this.lblBemVindo = new System.Windows.Forms.Label();
            this.gbMenu = new System.Windows.Forms.GroupBox();
            this.btnUser = new System.Windows.Forms.Button();
            this.btnFeedback = new System.Windows.Forms.Button();
            this.btnTopTen = new System.Windows.Forms.Button();
            this.btnStatistics = new System.Windows.Forms.Button();
            this.btnTrailer = new System.Windows.Forms.Button();
            this.btnSearchMovie = new System.Windows.Forms.Button();
            this.btnAdmin = new System.Windows.Forms.Button();
            this.btnSair = new System.Windows.Forms.Button();
            this.btnCompare = new System.Windows.Forms.Button();
            this.panelContainer = new System.Windows.Forms.Panel();
            this.gbLoged.SuspendLayout();
            this.gbMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbLoged
            // 
            this.gbLoged.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbLoged.Controls.Add(this.btnLogout);
            this.gbLoged.Controls.Add(this.txtUserLoged);
            this.gbLoged.Controls.Add(this.lblBemVindo);
            this.gbLoged.Location = new System.Drawing.Point(12, 12);
            this.gbLoged.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gbLoged.Name = "gbLoged";
            this.gbLoged.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gbLoged.Size = new System.Drawing.Size(917, 58);
            this.gbLoged.TabIndex = 1;
            this.gbLoged.TabStop = false;
            // 
            // btnLogout
            // 
            this.btnLogout.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnLogout.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLogout.Location = new System.Drawing.Point(803, 12);
            this.btnLogout.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnLogout.Name = "btnLogout";
            this.btnLogout.Size = new System.Drawing.Size(109, 39);
            this.btnLogout.TabIndex = 2;
            this.btnLogout.Text = "Logout";
            this.btnLogout.UseVisualStyleBackColor = true;
            this.btnLogout.Click += new System.EventHandler(this.BtnLogout_Click);
            // 
            // txtUserLoged
            // 
            this.txtUserLoged.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtUserLoged.AutoSize = true;
            this.txtUserLoged.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUserLoged.Location = new System.Drawing.Point(753, 18);
            this.txtUserLoged.Name = "txtUserLoged";
            this.txtUserLoged.Size = new System.Drawing.Size(0, 25);
            this.txtUserLoged.TabIndex = 1;
            // 
            // lblBemVindo
            // 
            this.lblBemVindo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblBemVindo.AutoSize = true;
            this.lblBemVindo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBemVindo.Location = new System.Drawing.Point(571, 18);
            this.lblBemVindo.Name = "lblBemVindo";
            this.lblBemVindo.Size = new System.Drawing.Size(177, 25);
            this.lblBemVindo.TabIndex = 0;
            this.lblBemVindo.Text = "Bem vindo de volta";
            // 
            // gbMenu
            // 
            this.gbMenu.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.gbMenu.Controls.Add(this.btnUser);
            this.gbMenu.Controls.Add(this.btnFeedback);
            this.gbMenu.Controls.Add(this.btnTopTen);
            this.gbMenu.Controls.Add(this.btnStatistics);
            this.gbMenu.Controls.Add(this.btnTrailer);
            this.gbMenu.Controls.Add(this.btnSearchMovie);
            this.gbMenu.Controls.Add(this.btnAdmin);
            this.gbMenu.Controls.Add(this.btnSair);
            this.gbMenu.Controls.Add(this.btnCompare);
            this.gbMenu.Location = new System.Drawing.Point(12, 76);
            this.gbMenu.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gbMenu.Name = "gbMenu";
            this.gbMenu.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gbMenu.Size = new System.Drawing.Size(252, 464);
            this.gbMenu.TabIndex = 2;
            this.gbMenu.TabStop = false;
            this.gbMenu.Text = "MENU";
            // 
            // btnUser
            // 
            this.btnUser.Location = new System.Drawing.Point(6, 348);
            this.btnUser.Margin = new System.Windows.Forms.Padding(4);
            this.btnUser.Name = "btnUser";
            this.btnUser.Size = new System.Drawing.Size(241, 43);
            this.btnUser.TabIndex = 8;
            this.btnUser.Text = "Conta Utilizador";
            this.btnUser.UseVisualStyleBackColor = true;
            // 
            // btnFeedback
            // 
            this.btnFeedback.Location = new System.Drawing.Point(6, 302);
            this.btnFeedback.Margin = new System.Windows.Forms.Padding(4);
            this.btnFeedback.Name = "btnFeedback";
            this.btnFeedback.Size = new System.Drawing.Size(241, 38);
            this.btnFeedback.TabIndex = 7;
            this.btnFeedback.Text = "Feedback";
            this.btnFeedback.UseVisualStyleBackColor = true;
            // 
            // btnTopTen
            // 
            this.btnTopTen.Location = new System.Drawing.Point(6, 253);
            this.btnTopTen.Margin = new System.Windows.Forms.Padding(4);
            this.btnTopTen.Name = "btnTopTen";
            this.btnTopTen.Size = new System.Drawing.Size(241, 43);
            this.btnTopTen.TabIndex = 6;
            this.btnTopTen.Text = "O meu Top 10";
            this.btnTopTen.UseVisualStyleBackColor = true;
            // 
            // btnStatistics
            // 
            this.btnStatistics.Location = new System.Drawing.Point(6, 206);
            this.btnStatistics.Margin = new System.Windows.Forms.Padding(4);
            this.btnStatistics.Name = "btnStatistics";
            this.btnStatistics.Size = new System.Drawing.Size(241, 39);
            this.btnStatistics.TabIndex = 5;
            this.btnStatistics.Text = "Estatísticas";
            this.btnStatistics.UseVisualStyleBackColor = true;
            // 
            // btnTrailer
            // 
            this.btnTrailer.Location = new System.Drawing.Point(6, 161);
            this.btnTrailer.Margin = new System.Windows.Forms.Padding(4);
            this.btnTrailer.Name = "btnTrailer";
            this.btnTrailer.Size = new System.Drawing.Size(241, 37);
            this.btnTrailer.TabIndex = 4;
            this.btnTrailer.Text = "Ver Trailer";
            this.btnTrailer.UseVisualStyleBackColor = true;
            // 
            // btnSearchMovie
            // 
            this.btnSearchMovie.Location = new System.Drawing.Point(6, 114);
            this.btnSearchMovie.Margin = new System.Windows.Forms.Padding(4);
            this.btnSearchMovie.Name = "btnSearchMovie";
            this.btnSearchMovie.Size = new System.Drawing.Size(241, 39);
            this.btnSearchMovie.TabIndex = 3;
            this.btnSearchMovie.Text = "Pesquisar Filme";
            this.btnSearchMovie.UseVisualStyleBackColor = true;
            this.btnSearchMovie.Click += new System.EventHandler(this.BtnSearchMovie_Click);
            // 
            // btnAdmin
            // 
            this.btnAdmin.Location = new System.Drawing.Point(6, 68);
            this.btnAdmin.Margin = new System.Windows.Forms.Padding(4);
            this.btnAdmin.Name = "btnAdmin";
            this.btnAdmin.Size = new System.Drawing.Size(241, 39);
            this.btnAdmin.TabIndex = 2;
            this.btnAdmin.Text = "Administrador";
            this.btnAdmin.UseVisualStyleBackColor = true;
            // 
            // btnSair
            // 
            this.btnSair.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSair.Location = new System.Drawing.Point(6, 409);
            this.btnSair.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnSair.Name = "btnSair";
            this.btnSair.Size = new System.Drawing.Size(241, 41);
            this.btnSair.TabIndex = 1;
            this.btnSair.Text = "Sair";
            this.btnSair.UseVisualStyleBackColor = true;
            this.btnSair.Click += new System.EventHandler(this.BtnSair_Click);
            // 
            // btnCompare
            // 
            this.btnCompare.Location = new System.Drawing.Point(6, 21);
            this.btnCompare.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnCompare.Name = "btnCompare";
            this.btnCompare.Size = new System.Drawing.Size(241, 41);
            this.btnCompare.TabIndex = 0;
            this.btnCompare.Text = "Comparar";
            this.btnCompare.UseVisualStyleBackColor = true;
            this.btnCompare.Click += new System.EventHandler(this.BtnCompare_Click);
            // 
            // panelContainer
            // 
            this.panelContainer.Location = new System.Drawing.Point(270, 76);
            this.panelContainer.Name = "panelContainer";
            this.panelContainer.Size = new System.Drawing.Size(659, 465);
            this.panelContainer.TabIndex = 3;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ClientSize = new System.Drawing.Size(941, 553);
            this.Controls.Add(this.panelContainer);
            this.Controls.Add(this.gbMenu);
            this.Controls.Add(this.gbLoged);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "MainForm";
            this.Text = "Mobies";
            this.gbLoged.ResumeLayout(false);
            this.gbLoged.PerformLayout();
            this.gbMenu.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.GroupBox gbLoged;
        private System.Windows.Forms.Label txtUserLoged;
        private System.Windows.Forms.Label lblBemVindo;
        private System.Windows.Forms.Button btnLogout;
        private System.Windows.Forms.GroupBox gbMenu;
        private System.Windows.Forms.Button btnCompare;
        private System.Windows.Forms.Button btnSair;
        private System.Windows.Forms.Button btnAdmin;
        private System.Windows.Forms.Button btnSearchMovie;
        private System.Windows.Forms.Button btnUser;
        private System.Windows.Forms.Button btnFeedback;
        private System.Windows.Forms.Button btnTopTen;
        private System.Windows.Forms.Button btnStatistics;
        private System.Windows.Forms.Button btnTrailer;
        private System.Windows.Forms.Panel panelContainer;
    }
}

