﻿using System.Collections;
using System.Text.RegularExpressions;

namespace Mobies.Functions
{
    static class Utils
    {
        private static readonly Regex rgx = new Regex("[^a-zA-Z0-9]");

        public static ArrayList RemoveNonAlphaNumericFromArray(ArrayList array)
        {
            ArrayList aux = new ArrayList();

            foreach (string line in array)
            {
                aux.Add(rgx.Replace(line.ToLowerInvariant(), ""));
            }

            return aux;
        }

        public static ArrayList GetMissingMovie(ArrayList auxOne, ArrayList auxTwo, ArrayList realList, string platform)
        {
            ArrayList missingMovie = new ArrayList();
            int i;

            foreach (string line in auxOne)
            {
                if (!auxTwo.Contains(line))
                {
                    for (i = 0; i < auxOne.Count; i++)
                    {
                        if (line.Equals(rgx.Replace(realList[i].ToString().ToLowerInvariant(), "")))
                        {
                            missingMovie.Add(realList[i].ToString() + " --> " + platform);
                        }
                    }
                }
            }

            return missingMovie;
        }
    }
}
