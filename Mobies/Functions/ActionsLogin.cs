﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mobies.Functions
{
    class ActionsLogin
    {
        private readonly ActionsFiles af = new ActionsFiles();

        public bool Login(string user, string pass)
        {
            bool isTrue = false;

            string userAccount = CheckForUsername(user, pass);

            if (!userAccount.Split('-')[0].Equals("") && !userAccount.Split('-')[1].Equals(""))
            {
                isTrue = true;
            } else
            {
                MessageBox.Show("Credênciais incorretas, tenta de novo.", "Login Errado", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            return isTrue;
        }

        public string CheckForUsername(string user, string pass)
        {
            string account = "";

            object[,] dbInformation = af.GetExcelDatabase();
            int columnCount = af.getColumnCount;

            for (int i = 1; i <= columnCount+1; i++)
            {
                if (user.Equals(dbInformation[i, 1]) && pass.Equals(dbInformation[i, 2]))
                {
                    account = dbInformation[i, 1].ToString() + "-";
                    for (int j = 2; j <= columnCount; j++)
                    {
                        account = account + dbInformation[i, j].ToString() + "-";
                    }
                }
            }

            account = account.Remove(account.Length - 1);

            return account;
        }
    }
}
