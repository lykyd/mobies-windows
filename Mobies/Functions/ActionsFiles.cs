﻿using Excel = Microsoft.Office.Interop.Excel;
using System;
using System.Collections;
using System.Runtime.InteropServices;
using System.IO;
using System.Linq;
using System.Collections.Generic;

namespace Mobies.Functions
{
    class ActionsFiles
    {
        private int rowCount;
        private int columnCount;
        readonly string workingDirectory = Directory.GetCurrentDirectory();

        public int getRowCount { get { return rowCount; } }
        public int getColumnCount { get { return columnCount; } }

        public ArrayList ReadExcel(string excelPath, string platform)
        {
            ArrayList difMovies = new ArrayList();
            Excel.Application xlApp = new Excel.Application();
            Excel.Workbook xlWorkbook = xlApp.Workbooks.Open(excelPath);
            Excel._Worksheet xlWorksheet = xlWorkbook.Sheets[1];
            Excel._Worksheet xlWorksheet2 = xlWorkbook.Sheets[2];
            Excel.Range xlRange = xlWorksheet.UsedRange;
            Excel.Range xlRange2 = xlWorksheet2.UsedRange;

            int rowCount = xlRange.Rows.Count;
            string numTotalMovies = xlRange2.Cells[2, 2].Value2.ToString();

            int i;
            for (i = 5; i <= Convert.ToInt32(numTotalMovies) + 4; i++)
            {
                if (platform.Contains(xlRange.Cells[i, 12].Value.ToString()))
                {
                    difMovies.Add(xlRange.Cells[i, 3].Value.ToString() + " " + xlRange.Cells[i, 4].Value.ToString());
                }
            }

            GC.Collect();
            GC.WaitForPendingFinalizers();

            Marshal.ReleaseComObject(xlRange);
            Marshal.ReleaseComObject(xlWorksheet);

            xlWorkbook.Close();
            Marshal.ReleaseComObject(xlWorkbook);

            xlApp.Quit();
            Marshal.ReleaseComObject(xlApp);

            return difMovies;
        }

        public List<string> ReadPath(string folderPath)
        {
            List<string> folders =
                new DirectoryInfo(folderPath)
                .EnumerateDirectories("*(*)", SearchOption.AllDirectories)
                .Select(d => d.Name).ToList();

            return folders;
        }

        public object[,] GetExcelDatabase()
        {
            string projectDirectory = Directory.GetParent(workingDirectory).Parent.FullName;

            Excel.Application xlApp = new Excel.Application();
            Excel.Workbook xlWorkbook = xlApp.Workbooks.Open(projectDirectory + Constants.DATABASE_FILE);
            Excel._Worksheet xlWorksheet = xlWorkbook.Sheets[1];
            Excel.Range xlRange = xlWorksheet.UsedRange;

            rowCount = xlRange.Rows.Count;
            columnCount = xlRange.Columns.Count;

            Excel.Range range = xlWorksheet.get_Range(Constants.FIRST_CELL, Constants.LAST_CELL + rowCount.ToString());
            object[,] dbInformation = (object[,])range.get_Value(Excel.XlRangeValueDataType.xlRangeValueDefault);

            GC.Collect();
            GC.WaitForPendingFinalizers();

            Marshal.ReleaseComObject(xlRange);
            Marshal.ReleaseComObject(xlWorksheet);

            xlWorkbook.Close();
            Marshal.ReleaseComObject(xlWorkbook);

            xlApp.Quit();
            Marshal.ReleaseComObject(xlApp);

            return dbInformation;
        }
    }
}
