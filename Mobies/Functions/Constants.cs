﻿namespace Mobies.Functions
{
    public static class Constants
    {
        public const string APP_VERSION = "v1.0";
        public const string APP_AUTOR = "Ivo Mendes";
        public const string DATABASE_FILE = "/Resources/database.xlsx";
        public const string FIRST_CELL = "A1";
        public const string LAST_CELL = "C";

        public const string EXCEL = "Excel";

        public const string MSG_BOX_COMPARE_TITLE = "Comparação de Filmes";
        public const string MSG_BOX_EMPTY_FIELDS_TITLE = "Campos vazios!";
        public const string MSG_BOX_MOVIES_OK_TITLE = "Filmes corretos!";
    }
}
