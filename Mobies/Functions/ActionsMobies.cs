﻿using System.Collections;
using System.Windows.Forms;
using System;

namespace Mobies.Functions
{
    class ActionsMobies
    {
        private readonly ActionsFiles af = new ActionsFiles();

        public ArrayList CompareMoviesLists(string folderPath, string excelPath, string platform)
        {
            ArrayList excelMovies = af.ReadExcel(excelPath, platform);
            ArrayList folderMovies = new ArrayList(af.ReadPath(folderPath));
            ArrayList auxExcelMovies = Utils.RemoveNonAlphaNumericFromArray(excelMovies);
            ArrayList auxFolderMovies = Utils.RemoveNonAlphaNumericFromArray(folderMovies);
            ArrayList difMovies = new ArrayList();
            bool isListValid = true;

            if (auxExcelMovies.Count != auxFolderMovies.Count)
            {
                MessageBox.Show("Filmes em Excel: " + auxExcelMovies.Count + Environment.NewLine + "Filmes na Pasta: " + auxFolderMovies.Count,
                    Constants.MSG_BOX_COMPARE_TITLE,
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                isListValid = false;
            }

            difMovies.AddRange(Utils.GetMissingMovie(auxExcelMovies, auxFolderMovies, excelMovies, platform));
            difMovies.AddRange(Utils.GetMissingMovie(auxFolderMovies, auxExcelMovies, folderMovies, Constants.EXCEL));

            if (auxExcelMovies.Count == auxFolderMovies.Count && difMovies.Count != 0)
            {
                MessageBox.Show("Número de filmes idêntico." + Environment.NewLine + "Mas existem filmes em falta.",
                    Constants.MSG_BOX_COMPARE_TITLE,
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                isListValid = false;
            }

            if (isListValid)
            {
                MessageBox.Show("Todos os filmes estão catalogados corretamente.",
                    Constants.MSG_BOX_MOVIES_OK_TITLE,
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
            }

            return difMovies;
        }
    }
}
