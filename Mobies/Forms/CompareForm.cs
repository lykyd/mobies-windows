﻿using Mobies.Functions;
using System;
using System.Collections;
using System.Windows.Forms;

namespace Mobies.Forms
{
    public partial class CompareForm : Form
    {

        private readonly ActionsMobies am = new ActionsMobies();

        public CompareForm()
        {
            InitializeComponent();
        }

        private void BtnStartSearch_Click(object sender, EventArgs e)
        {
            ArrayList difMovies;
            int i;

            if (!txtDifMovies.Text.Equals(string.Empty))
            {
                txtDifMovies.Text = "";
            }

            if(txtMoviesPath.Text.Equals(string.Empty) || txtExcelPath.Text.Equals(string.Empty) || cbPlataforma.Text.Equals(string.Empty))
            {
                MessageBox.Show("Alguns campos estão vazios!" + Environment.NewLine + "Por favor preencher correctamente.",
                    Constants.MSG_BOX_EMPTY_FIELDS_TITLE,
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Warning);
            }
            else
            {
                difMovies = am.CompareMoviesLists(txtMoviesPath.Text, txtExcelPath.Text, cbPlataforma.Text);

                for (i = 0; i < difMovies.Count; i++)
                {
                    txtDifMovies.AppendText(difMovies[i].ToString() + Environment.NewLine);
                }
                txtDifMovies.ScrollBars = ScrollBars.Vertical;
            }
        }

        private void BtnCloseCompare_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BtnSearchFolder_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog folderSearch = new FolderBrowserDialog();
            folderSearch.ShowDialog();
            txtMoviesPath.Text = folderSearch.SelectedPath;
        }

        private void BtnSearchExcel_Click(object sender, EventArgs e)
        {
            OpenFileDialog excelSearch = new OpenFileDialog();
            excelSearch.ShowDialog();
            txtExcelPath.Text = excelSearch.FileName;
        }
    }
}
