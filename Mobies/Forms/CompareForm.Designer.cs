﻿namespace Mobies.Forms
{
    partial class CompareForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtDifMovies = new System.Windows.Forms.TextBox();
            this.lblFilmesDifrentes = new System.Windows.Forms.Label();
            this.btnCloseCompare = new System.Windows.Forms.Button();
            this.btnStartSearch = new System.Windows.Forms.Button();
            this.lblPlataforma = new System.Windows.Forms.Label();
            this.cbPlataforma = new System.Windows.Forms.ComboBox();
            this.btnSearchExcel = new System.Windows.Forms.Button();
            this.txtExcelPath = new System.Windows.Forms.TextBox();
            this.btnSearchFolder = new System.Windows.Forms.Button();
            this.txtMoviesPath = new System.Windows.Forms.TextBox();
            this.lblFicheiroExcel = new System.Windows.Forms.Label();
            this.lblPastaFilmes = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // txtDifMovies
            // 
            this.txtDifMovies.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.txtDifMovies.Location = new System.Drawing.Point(15, 254);
            this.txtDifMovies.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtDifMovies.Multiline = true;
            this.txtDifMovies.Name = "txtDifMovies";
            this.txtDifMovies.ReadOnly = true;
            this.txtDifMovies.Size = new System.Drawing.Size(617, 242);
            this.txtDifMovies.TabIndex = 24;
            // 
            // lblFilmesDifrentes
            // 
            this.lblFilmesDifrentes.AutoSize = true;
            this.lblFilmesDifrentes.Location = new System.Drawing.Point(15, 233);
            this.lblFilmesDifrentes.Name = "lblFilmesDifrentes";
            this.lblFilmesDifrentes.Size = new System.Drawing.Size(152, 17);
            this.lblFilmesDifrentes.TabIndex = 23;
            this.lblFilmesDifrentes.Text = "Filmes com diferênças:";
            // 
            // btnCloseCompare
            // 
            this.btnCloseCompare.Location = new System.Drawing.Point(449, 153);
            this.btnCloseCompare.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnCloseCompare.Name = "btnCloseCompare";
            this.btnCloseCompare.Size = new System.Drawing.Size(181, 46);
            this.btnCloseCompare.TabIndex = 22;
            this.btnCloseCompare.Text = "Fechar Comparação";
            this.btnCloseCompare.UseVisualStyleBackColor = true;
            this.btnCloseCompare.Click += new System.EventHandler(this.BtnCloseCompare_Click);
            // 
            // btnStartSearch
            // 
            this.btnStartSearch.Location = new System.Drawing.Point(15, 153);
            this.btnStartSearch.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnStartSearch.Name = "btnStartSearch";
            this.btnStartSearch.Size = new System.Drawing.Size(380, 46);
            this.btnStartSearch.TabIndex = 21;
            this.btnStartSearch.Text = "COMEÇAR COMPARAÇÃO";
            this.btnStartSearch.UseVisualStyleBackColor = true;
            this.btnStartSearch.Click += new System.EventHandler(this.BtnStartSearch_Click);
            // 
            // lblPlataforma
            // 
            this.lblPlataforma.AutoSize = true;
            this.lblPlataforma.Location = new System.Drawing.Point(12, 103);
            this.lblPlataforma.Name = "lblPlataforma";
            this.lblPlataforma.Size = new System.Drawing.Size(80, 17);
            this.lblPlataforma.TabIndex = 20;
            this.lblPlataforma.Text = "Plataforma:";
            // 
            // cbPlataforma
            // 
            this.cbPlataforma.FormattingEnabled = true;
            this.cbPlataforma.Items.AddRange(new object[] {
            "PC",
            "Disco 1Tb",
            "Disco 2Tb"});
            this.cbPlataforma.Location = new System.Drawing.Point(137, 101);
            this.cbPlataforma.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cbPlataforma.Name = "cbPlataforma";
            this.cbPlataforma.Size = new System.Drawing.Size(161, 24);
            this.cbPlataforma.TabIndex = 19;
            // 
            // btnSearchExcel
            // 
            this.btnSearchExcel.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSearchExcel.Location = new System.Drawing.Point(593, 50);
            this.btnSearchExcel.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnSearchExcel.Name = "btnSearchExcel";
            this.btnSearchExcel.Size = new System.Drawing.Size(39, 23);
            this.btnSearchExcel.TabIndex = 18;
            this.btnSearchExcel.Text = "...";
            this.btnSearchExcel.UseVisualStyleBackColor = true;
            this.btnSearchExcel.Click += new System.EventHandler(this.BtnSearchExcel_Click);
            // 
            // txtExcelPath
            // 
            this.txtExcelPath.Location = new System.Drawing.Point(137, 51);
            this.txtExcelPath.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtExcelPath.Name = "txtExcelPath";
            this.txtExcelPath.Size = new System.Drawing.Size(436, 22);
            this.txtExcelPath.TabIndex = 17;
            // 
            // btnSearchFolder
            // 
            this.btnSearchFolder.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSearchFolder.Location = new System.Drawing.Point(593, 3);
            this.btnSearchFolder.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnSearchFolder.Name = "btnSearchFolder";
            this.btnSearchFolder.Size = new System.Drawing.Size(39, 23);
            this.btnSearchFolder.TabIndex = 16;
            this.btnSearchFolder.Text = "...";
            this.btnSearchFolder.UseVisualStyleBackColor = true;
            this.btnSearchFolder.Click += new System.EventHandler(this.BtnSearchFolder_Click);
            // 
            // txtMoviesPath
            // 
            this.txtMoviesPath.Location = new System.Drawing.Point(137, 5);
            this.txtMoviesPath.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtMoviesPath.Name = "txtMoviesPath";
            this.txtMoviesPath.Size = new System.Drawing.Size(436, 22);
            this.txtMoviesPath.TabIndex = 15;
            // 
            // lblFicheiroExcel
            // 
            this.lblFicheiroExcel.AutoSize = true;
            this.lblFicheiroExcel.Location = new System.Drawing.Point(12, 57);
            this.lblFicheiroExcel.Name = "lblFicheiroExcel";
            this.lblFicheiroExcel.Size = new System.Drawing.Size(99, 17);
            this.lblFicheiroExcel.TabIndex = 14;
            this.lblFicheiroExcel.Text = "Ficheiro Excel:";
            // 
            // lblPastaFilmes
            // 
            this.lblPastaFilmes.AutoSize = true;
            this.lblPastaFilmes.Location = new System.Drawing.Point(12, 9);
            this.lblPastaFilmes.Name = "lblPastaFilmes";
            this.lblPastaFilmes.Size = new System.Drawing.Size(119, 17);
            this.lblPastaFilmes.TabIndex = 13;
            this.lblPastaFilmes.Text = "Pasta dos Filmes:";
            // 
            // CompareForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(645, 508);
            this.Controls.Add(this.txtDifMovies);
            this.Controls.Add(this.lblFilmesDifrentes);
            this.Controls.Add(this.btnCloseCompare);
            this.Controls.Add(this.btnStartSearch);
            this.Controls.Add(this.lblPlataforma);
            this.Controls.Add(this.cbPlataforma);
            this.Controls.Add(this.btnSearchExcel);
            this.Controls.Add(this.txtExcelPath);
            this.Controls.Add(this.btnSearchFolder);
            this.Controls.Add(this.txtMoviesPath);
            this.Controls.Add(this.lblFicheiroExcel);
            this.Controls.Add(this.lblPastaFilmes);
            this.Name = "CompareForm";
            this.Text = "Compare";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtDifMovies;
        private System.Windows.Forms.Label lblFilmesDifrentes;
        private System.Windows.Forms.Button btnCloseCompare;
        private System.Windows.Forms.Button btnStartSearch;
        private System.Windows.Forms.Label lblPlataforma;
        private System.Windows.Forms.ComboBox cbPlataforma;
        private System.Windows.Forms.Button btnSearchExcel;
        private System.Windows.Forms.TextBox txtExcelPath;
        private System.Windows.Forms.Button btnSearchFolder;
        private System.Windows.Forms.TextBox txtMoviesPath;
        private System.Windows.Forms.Label lblFicheiroExcel;
        private System.Windows.Forms.Label lblPastaFilmes;
    }
}