﻿using Mobies.Functions;
using System;
using System.Windows.Forms;

namespace Mobies.Forms
{
    public partial class LoginForm : Form
    {
        private readonly ActionsLogin al = new ActionsLogin();
        private MainForm mf;

        public LoginForm()
        {
            InitializeComponent();
        }

        public void btnLogin_Click(object sender, EventArgs e)
        {
            bool isTrue;

            isTrue = al.Login(txtUser.Text, txtPass.Text);

            if (isTrue)
            {
                mf = new MainForm(txtUser.Text);
                mf.Show();
                this.Visible = false;
                txtUser.ResetText();
                txtPass.ResetText();
            }
            else
            {
                txtUser.ResetText();
                txtPass.ResetText();
            }
        }

        private void BtnRegister_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Funcionalidade não implementada", "AVISO!!!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }
    }
}
